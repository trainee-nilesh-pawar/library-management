package com.sunbeam.exceptions;

public class UnauthorizedUser extends RuntimeException {

	public UnauthorizedUser(String message) {
		super(message);
	}
}
