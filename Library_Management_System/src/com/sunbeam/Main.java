package com.sunbeam;

import java.util.Scanner;

import com.sunbeam.controller.BookController;
import com.sunbeam.controller.ConsoleMemberController;
import com.sunbeam.controller.GlobalController;
import com.sunbeam.controller.PaymentController;
import com.sunbeam.controller.RackController;
import com.sunbeam.dto.User;
import com.sunbeam.service.PaymentServiceImpl;
import com.sunbeam.dto.Books;
import com.sunbeam.dto.Member;


public class Main {
	public static User loggedInUser;
	public static Member loggedInMember;
	public static Books book;

	public static Scanner globalScanner;

	public static void main(String[] args) {
		globalScanner = new Scanner(System.in);
     
		
		do {
			System.out.println("********LIBRARY MANAGEMENT SYSTEM*******");
			System.out.println("Press 0 to Exit");
			System.out.println("Press 1 to Sign Up");
			System.out.println("Press 2 to Sign In");
			
			System.out.print("Select your option: ");
			int a = globalScanner.nextInt();
			switch (a) {
			case 0:
				System.out.println("Bye.");
				System.exit(0);
				break;
			case 1:
				GlobalController.Signup();
				break;
			case 2:
				loggedInUser = GlobalController.SignIn();
				break;
			default:
				System.out.println("Invalid choice");
			}
			
			do {
				boolean signOut = false;
				if(null != loggedInUser) {
					final String loggedInUserRole = loggedInUser.getRole();
					System.out.println("Logged in user's role is " + loggedInUserRole);
					
					System.out.println("********LIBRARY MANAGEMENT SYSTEM*******");
					System.out.println("Press 0 to Sign out");
					System.out.println("Press 1 to Edit profile");
					System.out.println("Press 2 to Change password");
					if("librarian".equalsIgnoreCase(loggedInUserRole) || 
							"member".equalsIgnoreCase(loggedInUserRole)) {
						System.out.println("Press 3 to Find book by name");
						System.out.println("Press 4 to Check Availability of book");
						if("librarian".equalsIgnoreCase(loggedInUserRole)) {
							System.out.println("Press 5 to Add book");
							System.out.println("Press 6 to Edit book");
							System.out.println("Press 7 to Add New Copy");
							System.out.println("Press 8 to user payment history");
							System.out.println("Press 8 to take payment");
						}
					}
					
					a = globalScanner.nextInt();
					switch (a) {
					case 0:
						signOut = true;
						break;
					case 1:
						GlobalController.EditProfile(loggedInUser);
						break;
					case 2:
						GlobalController.ChangePassword(loggedInUser.getId());
						break;
					case 3:
						BookController.FindBook();
						break;
					case 4:
						RackController.checkAvailability();
						break;
					case 5:
						BookController.AddBook();
						break;
					case 6:
						BookController.EditBook();
						break;
					case 7:
						RackController.AddBookCopy();
						break;
					case 8:
						PaymentController.paymentHistory();
					case 9:
						PaymentController.AddPayment();
					default:
						System.out.println("Invalid choice");
					}
				} else {
					// User is not signed in
					signOut = true;
				}
				if(signOut) {
					System.out.println("Logging out...");
					break;
				}
				System.out.println("\n");
			} while (true); // Role specific Menu
		} while (true); // Sign Up / Sign In Menu
	}
            
}
