package com.sunbeam.controller;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sunbeam.DBUtils;
import com.sunbeam.Main;
import com.sunbeam.dao.BookDao;
import com.sunbeam.dao.PaymentDao;
import com.sunbeam.dao.RackDao;
import com.sunbeam.dao.UserDao;
import com.sunbeam.dto.Books;
import com.sunbeam.dto.Payment;
import com.sunbeam.dto.Rack;
import com.sunbeam.service.LibrarianService;
import com.sunbeam.service.LibrarianServiceImpl;
import com.sunbeam.service.PaymentService;
import com.sunbeam.service.PaymentServiceImpl;
import com.sunbeam.service.RackService;
import com.sunbeam.service.RackServiceImpl;

public class PaymentController {

	public static void paymentHistory() {

		System.out.println("Enter userid : ");
		int userid = Main.globalScanner.nextInt();

		try {
			UserDao userDao = new UserDao(DBUtils.fetchConnection());
			PaymentDao paymentDao = new PaymentDao(DBUtils.fetchConnection());
			PaymentService paymentService = new PaymentServiceImpl(paymentDao);
			List<Payment> payments = paymentService.paymentHistory(userid);
			if (null == payments || payments.isEmpty()) {
				System.out.println("No payment history available with userId: " + userid);
			} else {

				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				String json = gson.toJson(payments);
				System.out.println("Found " + payments.size() + " payment history records for userId: " + userid);
				System.out.println(json);
			}

		} catch (Exception e) {
			System.out.println("Failed to fetch payment history, reason: " + e.getMessage());
		}
	}




public static void AddPayment() {	

	System.out.println("Enter Amount : ");
	double amount = Main.globalScanner.nextDouble();

	System.out.println("Enter userid : ");
	int userid = Main.globalScanner.nextInt();

	try {
		PaymentDao paymentDao = new PaymentDao(DBUtils.fetchConnection());
		PaymentService paymentService = new PaymentServiceImpl(paymentDao);
		Payment payment = paymentService.AddPayment(paymentDao);
		if(null == payment) {
			System.out.println("No book found with book Id: " + userid);
		} else {
			
		}
	} catch (Exception e) {
		
		e.printStackTrace();
	}

}
}
