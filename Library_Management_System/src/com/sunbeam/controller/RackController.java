package com.sunbeam.controller;

import java.util.List;

import com.google.gson.Gson;
import com.sunbeam.DBUtils;
import com.sunbeam.Main;
import com.sunbeam.dao.BookDao;
import com.sunbeam.dao.RackDao;
import com.sunbeam.dao.UserDao;
import com.sunbeam.dto.Books;
import com.sunbeam.dto.Rack;
import com.sunbeam.service.LibrarianService;
import com.sunbeam.service.LibrarianServiceImpl;
import com.sunbeam.service.RackService;
import com.sunbeam.service.RackServiceImpl;

public class RackController {
	
	public static void AddBookCopy() {	

		System.out.println("Enter Book Id : ");
		int bookId = Main.globalScanner.nextInt();

		System.out.println("Enter Rack : ");
		int rack = Main.globalScanner.nextInt();

		try {
			UserDao userDao = new UserDao(DBUtils.fetchConnection());
			BookDao bookDao = new BookDao(DBUtils.fetchConnection());
			LibrarianService librarianService = new LibrarianServiceImpl(userDao, bookDao);
			Books book = librarianService.FindBook(bookId);
			if(null == book) {
				System.out.println("No book found with book Id: " + bookId);
			} else {
				RackDao rackDao = new RackDao(DBUtils.fetchConnection());
				RackService rackService = new RackServiceImpl(rackDao);
				Rack rackCopy = rackService.AddBookCopy(new Rack(null, bookId, rack, true));
				System.out.println("Copy added successfully with ID :: " + rackCopy.getCopyid());
			}
		} catch (Exception e) {
			System.out.println("Failed to add copy, reason: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void checkAvailability() {

		System.out.println("Enter Book Id : ");
		int bookId = Main.globalScanner.nextInt();


		try {
			UserDao userDao = new UserDao(DBUtils.fetchConnection());
			BookDao bookDao = new BookDao(DBUtils.fetchConnection());
			LibrarianService librarianService = new LibrarianServiceImpl(userDao, bookDao);
			Books book = librarianService.FindBook(bookId);
			if(null == book) {
				System.out.println("No book found with book Id: " + bookId);
			} else {
				RackDao rackDao = new RackDao(DBUtils.fetchConnection());
				RackService rackService = new RackServiceImpl(rackDao);
				List<Rack> rackCopies = rackService.checkAvailability(bookId);
				if(null == rackCopies || rackCopies.isEmpty()) {
					System.out.println("No copies found with book Id: " + bookId);
				} else {
					System.out.println("Found " + rackCopies.size() + " copies with book Id: " + bookId);
					System.out.println(new Gson().toJson(rackCopies));
				}
			}
		} catch (Exception e) {
			System.out.println("Failed to add copy, reason: " + e.getMessage());
		}
	}

}
