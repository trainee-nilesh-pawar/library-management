package com.sunbeam.controller;

import java.util.List;

import com.google.gson.Gson;
import com.sunbeam.DBUtils;
import com.sunbeam.Main;
import com.sunbeam.dao.BookDao;
import com.sunbeam.dao.UserDao;
import com.sunbeam.dto.Books;
import com.sunbeam.exceptions.UnauthorizedUser;
import com.sunbeam.service.BookService;
import com.sunbeam.service.BookServiceImpl;
import com.sunbeam.service.LibrarianService;
import com.sunbeam.service.LibrarianServiceImpl;
import com.sunbeam.service.UserService;
import com.sunbeam.service.UserServiceImpl;

public class BookController {

	public static void AddBook() {

		Books book = new Books();		

		System.out.println("Enter Book name : ");
		String name = Main.globalScanner.next();
		book.setName(name);

		System.out.println("Enter Author name : ");
		String author = Main.globalScanner.next();
		book.setAuthor(author);

		System.out.println("Enter Subject : ");
		String subject = Main.globalScanner.next();
		book.setSubject(subject);

		System.out.println("Enter price : ");
		double price = Main.globalScanner.nextDouble(); 
		book.setPrice(price);

		System.out.println("Isbn number : ");
		String isbn = Main.globalScanner.next();
		book.setIsbn(isbn);

		try {
			UserDao userDao = new UserDao(DBUtils.fetchConnection());
			BookDao bookDao = new BookDao(DBUtils.fetchConnection());
			LibrarianService librarianService = new LibrarianServiceImpl(userDao, bookDao);
			Books bookid = librarianService.AddBook(book);

			System.out.println("Book added successfully with ID :: " + bookid);
		} catch (Exception e) {
			System.out.println("Unknown internal error");
		}
	}

	public static void EditBook() {
		

		System.out.println("Enter new Name :");
		String name = Main.globalScanner.next();

		System.out.println("New Author name :");
		String author = Main.globalScanner.next();
		
		System.out.println("New Subject name :");
		String subject = Main.globalScanner.next();
		
		System.out.println("New Price :");
		Double price = Main.globalScanner.nextDouble();
		
		System.out.println("New isbn number :");
		String isbn = Main.globalScanner.next();
		
		System.out.println("Enter Bookid to edit :");
		Integer bookid = Main.globalScanner.nextInt();

		try {
			UserDao userDao = new UserDao(DBUtils.fetchConnection());
			BookDao bookDao = new BookDao(DBUtils.fetchConnection());
			LibrarianService librarianService = new LibrarianServiceImpl(userDao, bookDao);

			Books book = new Books();
			book.setName(name);
			book.setAuthor(author);
			book.setSubject(subject);
			book.setPrice(price);
			book.setIsbn(isbn);
			book.setBookid(bookid);
			librarianService.EditBook(book);
                       	   
          
			System.out.println("Book Updated successfully");
		} catch (Exception e) {
			System.out.println("Unknown internal error");
			e.printStackTrace();
		}
		

	}
	
	public static Books FindBook() {

		System.out.println("Enter Book name :");
		String name = Main.globalScanner.next();

		try {
			UserDao userDao = new UserDao(DBUtils.fetchConnection());
			BookDao bookDao = new BookDao(DBUtils.fetchConnection());
			UserService userService = new UserServiceImpl(userDao, bookDao);

			List<Books> books = userService.FindBook(name);
			System.out.println("Found these books:");
			System.out.println(new Gson().toJson(books));
		} catch (UnauthorizedUser e) {
			System.out.println("Invalid login..");
		} catch (Exception e) {
			System.out.println("Unknown internal error");
		}

		return null;
	}
}


