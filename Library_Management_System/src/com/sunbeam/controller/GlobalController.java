package com.sunbeam.controller;

import java.util.Scanner;

import com.sunbeam.DBUtils;
import com.sunbeam.Main;
import com.sunbeam.dao.UserDao;
import com.sunbeam.dto.User;
import com.sunbeam.exceptions.UnauthorizedUser;
import com.sunbeam.service.GlobalService;
import com.sunbeam.service.GlobalServiceImpl;
import com.sunbeam.service.UserService;
import com.sunbeam.service.UserServiceImpl;

public class GlobalController {

	public static User SignIn() {

		System.out.println("Enter EmailId :");
		String emailId = Main.globalScanner.next();

		System.out.println("Enter password:");
		String password = Main.globalScanner.next();

		try {
			UserDao userDao = new UserDao(DBUtils.fetchConnection());
			GlobalService globalService = new GlobalServiceImpl(userDao);
			User u = globalService.login(emailId, password);
			System.out.println("login successful.\n");
			return u;
		} catch (UnauthorizedUser e) {
			System.out.println("Invalid login..");
		} catch (Exception e) {
			System.out.println("Unknown internal error");
		}

		return null;
	}

	public static void Signup() {

		User u = new User();

		System.out.println("Enter name : ");
		String name = Main.globalScanner.next();
		u.setName(name);

		System.out.println("Enter emailId : ");
		String emailId = Main.globalScanner.next();
		u.setEmail(emailId);

		System.out.println("Enter password : ");
		String password = Main.globalScanner.next();
		u.setPassword(password);

		System.out.println("Select role : ");
		System.out.println("Press 1 for Owner");
		System.out.println("Press 2 for Librarian");
		System.out.println("Press 3 for Member");
		
		System.out.print("Select your option: ");
		int a = Main.globalScanner.nextInt();
		String role = null;
		switch (a) {
		case 1:
			role = "owner";
			break;
		case 2:
			role = "librarian";
			break;
		case 3:
			role = "member";
			break;

		default:
			System.out.println("Invalid choice");
		}
		u.setRole(role);

		System.out.println("phone number : ");
		String phoneNo = Main.globalScanner.next();
		u.setPhone(phoneNo);

		try {
			UserDao userDao = new UserDao(DBUtils.fetchConnection());
			GlobalService globalService = new GlobalServiceImpl(userDao);
			Integer userID = globalService.Signup(u);

			System.out.println("New User Registered with ID :: " + userID);
		} catch (Exception e) {
			System.out.println("Unknown internal error");
		}
	}

	public static void EditProfile(User user) {
		if (user == null) {
			System.out.println("Please login first ...");
			return;
		}

		System.out.println("Enter new EmailId :");
		String emailId = Main.globalScanner.next();

		System.out.println("New phone number :");
		String phoneNo = Main.globalScanner.next();

		try {
			UserDao userDao = new UserDao(DBUtils.fetchConnection());
			GlobalService globalService = new GlobalServiceImpl(userDao);

			User u = new User();
			u.setEmail(emailId);
			u.setPhone(phoneNo);
			u.setId(user.getId());
			globalService.EditProfile(u);

			System.out.println("User Updated successfully");
		} catch (Exception e) {
			System.out.println("Unknown internal error");
		}

	}

	public static void ChangePassword(int userId) {

		System.out.println("Enter new Password :");
		String password = Main.globalScanner.next();

		try {
			UserDao userDao = new UserDao(DBUtils.fetchConnection());
			GlobalService globalService = new GlobalServiceImpl(userDao);

			globalService.ChangePassword(password, userId);

			System.out.println("Password Updated successfully");
		} catch (Exception e) {
			System.out.println("Unknown internal error");
		}

	}
}