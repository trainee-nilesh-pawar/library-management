package com.sunbeam.service;

import java.util.List;

import com.sunbeam.dto.Books;

public interface BookService {

	Books AddBook(Books book);
	void EditBook(Books book);
	List<Books> FindBook(String name);
	 
}
