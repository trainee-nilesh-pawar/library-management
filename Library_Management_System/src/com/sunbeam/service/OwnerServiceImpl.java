package com.sunbeam.service;

import java.util.Optional;

import com.sunbeam.dao.BookDao;
import com.sunbeam.dao.UnknownDataAccessException;
import com.sunbeam.dao.UserDao;
import com.sunbeam.dto.Books;
import com.sunbeam.dto.User;
import com.sunbeam.exceptions.UnauthorizedUser;

public class OwnerServiceImpl extends GlobalServiceImpl implements OwnerService {

	public OwnerServiceImpl(UserDao userDao) {
		super(userDao);
	}
}
