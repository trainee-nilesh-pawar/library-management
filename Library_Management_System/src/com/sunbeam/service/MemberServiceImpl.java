package com.sunbeam.service;


import java.util.Optional;

import com.sunbeam.dao.BookDao;
import com.sunbeam.dao.MemberDao;
import com.sunbeam.dao.UnknownDataAccessException;
import com.sunbeam.dao.UserDao;
import com.sunbeam.dto.Member;
import com.sunbeam.exceptions.UnauthorizedUser;

public class MemberServiceImpl extends UserServiceImpl implements MemberService {

	private MemberDao memberDao;

	public MemberServiceImpl(UserDao userDao, BookDao bookDao, MemberDao memberDao) {
		super(userDao, bookDao);
		this.memberDao = memberDao;
	}
}