package com.sunbeam.service;

import java.util.List;

import com.sunbeam.dao.BookDao;
import com.sunbeam.dto.Books;

public class BookServiceImpl implements BookService {
   
	private BookDao bookDao;

	public BookServiceImpl(BookDao bookDao) {
		this.bookDao = bookDao;
	}

	@Override
	public Books AddBook(Books book) {
		try {
			return bookDao.AddBook(book);
		} catch (Exception e) {
		throw e;	
		}
		
	}

	@Override
	public void EditBook(Books book) {
            try {
			    bookDao.EditBook(book);
			} catch (Exception e) {
		      throw e;
			}
		
	}

	@Override
	public List<Books> FindBook(String name) {
            try {
				return bookDao.FindBook(name);
			} catch (Exception e) {
				throw e;
			}
		
	}
}
