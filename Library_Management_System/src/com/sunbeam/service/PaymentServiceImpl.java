package com.sunbeam.service;

import java.util.List;

import com.sunbeam.dao.PaymentDao;
import com.sunbeam.dto.Payment;
import com.sunbeam.dto.Rack;

public class PaymentServiceImpl implements PaymentService {
	
	
	public PaymentServiceImpl(PaymentDao paymentDao) {
		super();
		this.paymentDao = paymentDao;
	}

	protected PaymentDao paymentDao;

	@Override
	public List<Payment> paymentHistory(int userid) {
		try {
			return paymentDao.paymentHistory(userid);
		} catch (Exception e) {
			throw e;
		}
		
	}
	
	@Override
	public Payment AddPayment(PaymentDao paymentDao) {
		try {
			return paymentDao.AddPayment(AddPayment(null));
		} catch (Exception e) {
			throw e;	
		}
	}

	
	
	
	
}
