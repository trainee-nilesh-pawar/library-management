package com.sunbeam.service;

import java.util.List;

import com.sunbeam.dto.Books;
import com.sunbeam.dto.User;
import com.sunbeam.exceptions.UnauthorizedUser;

public interface UserService extends GlobalService {
	
	Books FindBook(int id);
	
	List<Books> FindBook(String name);

}
