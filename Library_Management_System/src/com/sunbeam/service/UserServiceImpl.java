package com.sunbeam.service;

import java.util.List;
import java.util.Optional;

import com.sunbeam.dao.BookDao;
import com.sunbeam.dao.UnknownDataAccessException;
import com.sunbeam.dao.UserDao;
import com.sunbeam.dto.Books;
import com.sunbeam.dto.User;
import com.sunbeam.exceptions.UnauthorizedUser;

public class UserServiceImpl extends GlobalServiceImpl implements UserService {
	   
	protected BookDao bookDao;

	public UserServiceImpl(UserDao userDao, BookDao bookDao) {
		super(userDao);
		this.bookDao = bookDao;
	}

	@Override
	public Books FindBook(int id) {
            try {
				return bookDao.FindBook(id);
			} catch (Exception e) {
				throw e;
			}
		
	}

	@Override
	public List<Books> FindBook(String name) {
            try {
				return bookDao.FindBook(name);
			} catch (Exception e) {
				throw e;
			}
		
	}
}
