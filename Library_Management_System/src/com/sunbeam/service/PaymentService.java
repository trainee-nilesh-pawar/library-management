package com.sunbeam.service;

import java.util.List;

import com.sunbeam.dao.PaymentDao;
import com.sunbeam.dto.Payment;

public interface PaymentService {

	List<Payment> paymentHistory(int userid);
	
	public Payment AddPayment(PaymentDao paymentDao);
	
}
