package com.sunbeam.service;

import java.util.Optional;

import com.sunbeam.dao.BookDao;
import com.sunbeam.dao.UnknownDataAccessException;
import com.sunbeam.dao.UserDao;
import com.sunbeam.dto.Books;
import com.sunbeam.dto.Member;
import com.sunbeam.dto.User;
import com.sunbeam.exceptions.UnauthorizedUser;

public class GlobalServiceImpl implements GlobalService {

	protected UserDao userDao;

	public GlobalServiceImpl(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public Integer Signup(User user) {
		try {
			return userDao.Add(user).getId();
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public User login(String email, String password) throws UnauthorizedUser {
		try {
			Optional<User> u = userDao.FindByEmailAndPassword(email, password);
			if (u.isPresent()) {
				return u.get(); 
			}
			throw new UnauthorizedUser("User not exists");
		} catch (UnknownDataAccessException e) {
			throw e;
		}
	}

	@Override
	public void EditProfile(User user) throws UnauthorizedUser {
		try {
			userDao.Update(user);

		} catch (UnknownDataAccessException e) {
			throw e;
		}
	}

	@Override
	public void ChangePassword(String password, int userId) throws UnauthorizedUser {
		try {
			userDao.ChangePassword(password, userId);

		} catch (UnknownDataAccessException e) {
			throw e;
		}
	}
}
