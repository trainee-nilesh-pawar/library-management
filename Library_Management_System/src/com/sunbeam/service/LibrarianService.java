package com.sunbeam.service;

import com.sunbeam.dto.Books;

public interface LibrarianService extends UserService {
	
	Books AddBook(Books book);
	void EditBook(Books book);
	
	// void issueCopy(Books book);
	// void returnCopy(Books book);
	// void addNewCopy(Books book);
	// Member addNewMember(User user);
	// void changeRack(Books book);
	// void takePayment(Books book);
}
