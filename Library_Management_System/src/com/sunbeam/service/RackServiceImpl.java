package com.sunbeam.service;

import java.util.List;

import com.sunbeam.dao.BookDao;
import com.sunbeam.dao.RackDao;
import com.sunbeam.dao.UserDao;
import com.sunbeam.dto.Rack;

public class RackServiceImpl implements RackService {
	   
	protected RackDao rackDao;

	public RackServiceImpl(RackDao rackDao) {
		this.rackDao = rackDao;
	}

	@Override
	public Rack AddBookCopy(Rack rack) {
		try {
			return rackDao.AddBookCopy(rack);
		} catch (Exception e) {
			throw e;	
		}
	}

	@Override
	public List<Rack> checkAvailability(int bookId) {
		try {
			return rackDao.checkAvailability(bookId);
		} catch (Exception e) {
			throw e;	
		}
	}
}
