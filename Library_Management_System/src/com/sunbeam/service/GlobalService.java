package com.sunbeam.service;

import com.sunbeam.dto.Member;
import com.sunbeam.dto.User;
import com.sunbeam.exceptions.UnauthorizedUser;

public interface GlobalService {

	Integer Signup(User user);
	
	User login(String email, String password) throws UnauthorizedUser;

	void EditProfile(User user);

	void ChangePassword(String password, int userId);
}
