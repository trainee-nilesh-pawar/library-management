package com.sunbeam.service;

import java.util.List;

import com.sunbeam.dto.Rack;

public interface RackService {
	
	Rack AddBookCopy(Rack rack);

	List<Rack> checkAvailability(int bookId);
}

