package com.sunbeam.service;

import java.util.List;
import java.util.Optional;

import com.sunbeam.dao.BookDao;
import com.sunbeam.dao.UnknownDataAccessException;
import com.sunbeam.dao.UserDao;
import com.sunbeam.dto.Books;
import com.sunbeam.dto.User;
import com.sunbeam.exceptions.UnauthorizedUser;

public class LibrarianServiceImpl extends UserServiceImpl implements LibrarianService {

	public LibrarianServiceImpl(UserDao userDao, BookDao bookDao) {
		super(userDao, bookDao);
	}

	@Override
	public Books AddBook(Books book) {
		try {
			return bookDao.AddBook(book);
		} catch (Exception e) {
		throw e;	
		}
		
	}

	@Override
	public void EditBook(Books book) {
            try {
			    bookDao.EditBook(book);
			} catch (Exception e) {
		      throw e;
			}
		
	}
}
