package com.sunbeam.dto;

public class Rack {

	
	private int copyid;
	private int bookid;
	private int rack;
	private boolean status;
	
	
	public Rack(Integer copyid, int bookid, int rack, boolean status) {
		super();
		if(null != copyid) {
			this.copyid = copyid;
		}
		this.bookid = bookid;
		this.rack = rack;
		this.status = status;
	}


	public Rack() {
		// empty constructor
	}


	public int getCopyid() {
		return copyid;
	}


	public void setCopyid(int copyid) {
		this.copyid = copyid;
	}


	public int getBookid() {
		return bookid;
	}


	public void setBookid(int bookid) {
		this.bookid = bookid;
	}


	public int getRack() {
		return rack;
	}


	public void setRack(int rack) {
		this.rack = rack;
	}


	public boolean getStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	@Override
	public String toString() {
		return "RackDao [copyid=" + copyid + ", bookid=" + bookid + ", rack=" + rack + ", status=" + status + "]";
	}
	
	
	
	
}


