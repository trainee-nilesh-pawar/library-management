package com.sunbeam.dto;

public class Books {

	
	private int bookid;
	private String name;
	private String author;
	private String subject;
	private double price;
	private String isbn;
	
	
	
	
	public Books AddBook() {
		return null;
		
	}
	public Books(int bookid, String name, String author, String subject, double price, String isbn) {
		super();
		this.bookid = bookid;
		this.name = name;
		this.author = author;
		this.subject = subject;
		this.price = price;
		this.isbn = isbn;
	}
	public Books() {
		// TODO Auto-generated constructor stub
	}
	public int getBookid() {
		return bookid;
	}
	public void setBookid(int bookid) {
		this.bookid = bookid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	@Override
	public String toString() {
		return "Books [bookid=" + bookid + ", name=" + name + ", author=" + author + ", subject=" + subject + ", price="
				+ price + ", isbn=" + isbn + "]";
	}
	
	
	
	
}
