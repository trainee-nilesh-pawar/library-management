package com.sunbeam.dto;

import java.sql.*;
public class Payment {

	
	private int id;
	private int userid;
	private double amount;
	private String type;
	private Timestamp transaction_time; 
	private Date nextpayment_duedate;
	
	
	public Payment(int id, int userid, double amount, String type, Timestamp transaction_time,
			Date nextpayment_duedate) {
		super();
		this.id = id;
		this.userid = userid;
		this.amount = amount;
		this.type = type;
		this.transaction_time = transaction_time;
		this.nextpayment_duedate = nextpayment_duedate;
	}
	

	public Payment() {
		// empty constructor
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getUserid() {
		return userid;
	}


	public void setUserid(int userid) {
		this.userid = userid;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Timestamp getTransaction_time() {
		return transaction_time;
	}


	public void setTransaction_time(Timestamp transaction_time) {
		this.transaction_time = transaction_time;
	}


	public Date getNextpayment_duedate() {
		return nextpayment_duedate;
	}


	public void setNextpayment_duedate(Date nextpayment_duedate) {
		this.nextpayment_duedate = nextpayment_duedate;
	}


	@Override
	public String toString() {
		return "Payments [id=" + id + ", userid=" + userid + ", amount=" + amount + ", type=" + type
				+ ", transaction_time=" + transaction_time + ", nextpayment_duedate=" + nextpayment_duedate + "]";
	}
	
	
	
	
	
}
