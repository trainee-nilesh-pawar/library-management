package com.sunbeam.dao;

public class UnknownDataAccessException extends RuntimeException {

	private String message;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnknownDataAccessException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "UnknownDataAccessException [error =" + message + "]";
	}

}
