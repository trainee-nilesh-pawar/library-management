package com.sunbeam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sunbeam.dto.Books;
import com.sunbeam.dto.Rack;

public class RackDao {

	private Connection dbCon;
	
	public RackDao(Connection dbCon) {
		this.dbCon = dbCon;
   }
	
	public Rack AddBookCopy(Rack rack) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		int copyId = 0;
    
		try {
			myStmt = dbCon.prepareStatement("insert into copies(bookid,rack,status) values(?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			
			
			myStmt.setInt(1, rack.getBookid());
			myStmt.setInt(2, rack.getRack());
			myStmt.setBoolean(3, rack.getStatus());
	
			int rowAffected = myStmt.executeUpdate();
			if (rowAffected == 0) {
				throw new UnknownDataAccessException("failed to add copy to rack");
			}
	
			// get copy id
			rs = myStmt.getGeneratedKeys();
			if (rs.next())
				copyId = rs.getInt(1);
	
			if (copyId == 0) {
				throw new UnknownDataAccessException("failed to add copy to rack");
			}
	
			rack.setCopyid(copyId);
			return rack;
		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}
	}

	public List<Rack> checkAvailability(int bookId) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		int copyId = 0;
    
		try {
			myStmt = dbCon.prepareStatement("SELECT * FROM copies WHERE bookid = ? AND status = true");
			myStmt.setInt(1, bookId);
	
			rs = myStmt.executeQuery();

			List<Rack> l = new ArrayList<Rack>();
			while (rs.next()) {
				Rack rack = new Rack();
				rack.setCopyid(rs.getInt("copyid"));
				rack.setBookid(rs.getInt("bookid"));
				rack.setRack(rs.getInt("rack"));
				rack.setStatus(rs.getBoolean("status"));

				l.add(rack);
			}
			return l;
		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}
	}
}
