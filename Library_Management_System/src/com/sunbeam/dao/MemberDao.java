package com.sunbeam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

import com.sunbeam.dto.Member;

public class MemberDao {
	
	private Connection dbCon;
	
	public MemberDao(Connection dbCon) {
		this.dbCon = dbCon;
    }
	
	public Optional<Member> FindByEmailAndPassword(final String email, final String password) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		try {
			myStmt = dbCon.prepareStatement("select * from users where email = ? and password = ? limit 1");
			myStmt.setString(1, email);
			myStmt.setString(2, password);

			rs = myStmt.executeQuery();

			while (rs.next()) {
				Member u = new Member();
				u.setId(rs.getInt("USERID"));
				u.setEmail(rs.getString("EMAIL"));
				u.setPassword(rs.getString("PASSWORD"));
				u.setName(rs.getString("NAME"));
				u.setRole(rs.getString("ROLE"));
				u.setPhone(rs.getString("PHONE"));

				return Optional.of(u);
			}
		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}

		return Optional.empty();

	}

	public Member Add(Member member) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		int userID = 0;

		try {
			myStmt = dbCon.prepareStatement("insert into users(NAME,EMAIL,PHONE,PASSWORD,ROLE) values(?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			myStmt.setString(1, member.getName());
			myStmt.setString(2, member.getEmail());
			myStmt.setString(3, member.getPhone());
			myStmt.setString(4, member.getPassword());
			myStmt.setString(5, member.getRole());

			int rowAffected = myStmt.executeUpdate();
			if (rowAffected == 0) {
				throw new UnknownDataAccessException("failed to add user to store");
			}

			// get user id
			rs = myStmt.getGeneratedKeys();
			if (rs.next())
				userID = rs.getInt(1);

			if (userID == 0) {
				throw new UnknownDataAccessException("failed to add user to store");
			}

			member.setId(userID);
			return member;
		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}
	}

	public void Update(Member member) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		int userID = 0;
		int paramCount = 0;
		try {
			boolean flag = false;
			String s = "update users set";
			if (member.getEmail() != null) {
				s = s + " email = ?,";
				flag = true;
				paramCount++;
			}
			if (member.getPhone() != null) {
				s = s + " phone = ?,";
				flag = true;
				paramCount++;
			}
			if (flag = true) {

				s = s.substring(0, s.length() - 1);

				s = s + " Where userId = ?";
			}

			System.out.println("query = " + s);

			myStmt = dbCon.prepareStatement(s);
			if (member.getEmail() != null) {
				myStmt.setString(1, member.getEmail());
			}
			if (member.getPhone() != null) {
				myStmt.setString(2, member.getPhone());
			}
			myStmt.setInt(++paramCount, member.getId());

			myStmt.executeUpdate();

		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}
	}
	
	
	
}
