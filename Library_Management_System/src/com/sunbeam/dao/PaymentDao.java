package com.sunbeam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.sunbeam.dto.Payment;
import com.sunbeam.dto.Rack;

public class PaymentDao {

	private Connection dbCon;

	public PaymentDao(Connection dbCon) {
		this.dbCon = dbCon;
	}

	public List<Payment> paymentHistory(int userid) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		int id = 0;

		try {
			myStmt = dbCon.prepareStatement("SELECT * FROM payments WHERE userid = ?");
			myStmt.setInt(1, userid);

			rs = myStmt.executeQuery();

			List<Payment> l = new ArrayList<Payment>();
			while (rs.next()) {
				Payment payment = new Payment();
				payment.setAmount(rs.getDouble("amount"));
				payment.setType(rs.getString("type"));
				payment.setTransaction_time(rs.getTimestamp("transaction_time"));
				payment.setNextpayment_duedate(rs.getDate("nextpayment_duedate"));

				l.add(payment);
			}
			return l;
		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}
	}

	public Payment AddPayment(Payment payment) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		int copyId = 0;

		try {
			myStmt = dbCon.prepareStatement("insert into payments(userid,amount,type,transaction_time) values(?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);

			myStmt.setInt(1, payment.getUserid());
			myStmt.setDouble(2, payment.getAmount());
			myStmt.setString(3, payment.getType());
			myStmt.setTimestamp(4, new Timestamp(System.currentTimeMillis()));

			
			int rowAffected = myStmt.executeUpdate();
			if (rowAffected == 0) {
				throw new UnknownDataAccessException("failed to add payment");
			}

			// get copy id
			rs = myStmt.getGeneratedKeys();
			if (rs.next())
				copyId = rs.getInt(1);

			if (copyId == 0) {
				throw new UnknownDataAccessException("failed to add payment");
			}

			payment.setId(copyId);
			return payment;
		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}

	}
}
