package com.sunbeam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

import com.sunbeam.dto.Books;
import com.sunbeam.dto.User;

public class UserDao {

	private Connection dbCon;

	public UserDao(Connection dbCon) {
		this.dbCon = dbCon;
	}

	public Optional<User> FindByEmailAndPassword(final String email, final String password) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		try {
			myStmt = dbCon.prepareStatement("select * from users where email = ? and password = ? limit 1");
			myStmt.setString(1, email);
			myStmt.setString(2, password);

			rs = myStmt.executeQuery();

			while (rs.next()) {
				User u = new User();
				u.setId(rs.getInt("USERID"));
				u.setEmail(rs.getString("EMAIL"));
				u.setPassword(rs.getString("PASSWORD"));
				u.setName(rs.getString("NAME"));
				u.setRole(rs.getString("ROLE"));
				u.setPhone(rs.getString("PHONE"));

				return Optional.of(u);
			}
		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}

		return Optional.empty();

	}

	public User Add(User user) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		int userID = 0;

		try {
			myStmt = dbCon.prepareStatement("insert into users(NAME,EMAIL,PHONE,PASSWORD,ROLE) values(?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			myStmt.setString(1, user.getName());
			myStmt.setString(2, user.getEmail());
			myStmt.setString(3, user.getPhone());
			myStmt.setString(4, user.getPassword());
			myStmt.setString(5, user.getRole());

			int rowAffected = myStmt.executeUpdate();
			if (rowAffected == 0) {
				throw new UnknownDataAccessException("failed to add user to store");
			}

			// get user id
			rs = myStmt.getGeneratedKeys();
			if (rs.next())
				userID = rs.getInt(1);

			if (userID == 0) {
				throw new UnknownDataAccessException("failed to add user to store");
			}

			user.setId(userID);
			return user;
		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}
	}

	public void Update(User user) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		int userID = 0;
		int paramCount = 0;
		try {
			boolean flag = false;
			String s = "update users set";
			if (user.getEmail() != null) {
				s = s + " email = ?,";
				flag = true;
				paramCount++;
			}
			if (user.getPhone() != null) {
				s = s + " phone = ?,";
				flag = true;
				paramCount++;
			}
			if (flag = true) {

				s = s.substring(0, s.length() - 1);

				s = s + " Where userId = ?";
			}

			myStmt = dbCon.prepareStatement(s);
			if (user.getEmail() != null) {
				myStmt.setString(1, user.getEmail());
			}
			if (user.getPhone() != null) {
				myStmt.setString(2, user.getPhone());
			}
			myStmt.setInt(++paramCount, user.getId());

			myStmt.executeUpdate();

		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}
	}

	public void ChangePassword(String password, int userId) {
		PreparedStatement myStmt;
		ResultSet rs = null;

		try {

			myStmt = dbCon.prepareStatement("UPDATE USERS SET PASSWORD = ? WHERE USERID = ?");
			myStmt.setString(1, password);
			myStmt.setInt(2, userId);

			myStmt.executeUpdate();

		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}
	}

	
}

