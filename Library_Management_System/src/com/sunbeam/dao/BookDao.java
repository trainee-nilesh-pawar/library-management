package com.sunbeam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.sunbeam.dto.Books;
import com.sunbeam.dto.User;

public class BookDao {
 
	private Connection dbCon;

	public BookDao(Connection dbCon) {
		this.dbCon = dbCon;
	}
	
	public Books AddBook(Books book) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		int bookid = 0;
		

		try {
			myStmt = dbCon.prepareStatement("insert into books(NAME,AUTHOR,SUBJECT,PRICE,ISBN) values(?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			
			myStmt.setString(1, book.getName());
			myStmt.setString(2, book.getAuthor());
			myStmt.setString(3, book.getSubject());
			myStmt.setDouble(4, book.getPrice());
			myStmt.setString(5, book.getIsbn());

			int rowAffected = myStmt.executeUpdate();
			if (rowAffected == 0) {
				throw new UnknownDataAccessException("failed to add book to store");
			}

			// get user id
			rs = myStmt.getGeneratedKeys();
			if (rs.next())
				bookid = rs.getInt(1);

			if (bookid == 0) {
				throw new UnknownDataAccessException("failed to add user to store");
			}

			book.setBookid(bookid);
			return book;
		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}
	}
	
	public void EditBook(Books book) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		int bookid = 0;
		int paramCount = 0;
		try {
			boolean flag = false;
			String s = "update books set";
			double b;
			
			if (book.getName() != null) {
				s = s + " name = ?,";
				flag = true;
				paramCount++;
			}
			if (book.getAuthor() != null) {
				s = s + " author = ?,";
				flag = true;
				paramCount++;
			}
			if (book.getSubject() != null) {
				s = s + " subject = ?,";
				flag = true;
				paramCount++;
			}
			if (book.getPrice() != 0) {
				s = s +  " price = ?,";
				flag = true;
				paramCount++;
			}
			if (book.getIsbn() != null) {
				s = s + " isbn = ?,";
				flag = true;
				paramCount++;
			}
			if (flag = true) {

				s = s.substring(0, s.length() - 1);

				s = s + " Where bookid = ?";
			}

			System.out.println("query = " + s);

			myStmt = dbCon.prepareStatement(s);
			if (book.getName() != null) {
				myStmt.setString(1, book.getName());
			}
			if (book.getAuthor() != null) {
				myStmt.setString(2, book.getAuthor());
			}
			if (book.getSubject() != null) {
				myStmt.setString(3, book.getSubject());
			}
			if (book.getPrice() != 0) {
				myStmt.setDouble(4, book.getPrice());
			}
			if (book.getIsbn() != null) {
				myStmt.setString(5, book.getIsbn());
			}
			myStmt.setInt(++paramCount, book.getBookid());

			myStmt.executeUpdate();

		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}
	} 
	
	public Books FindBook(final int id) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		try {
			myStmt = dbCon.prepareStatement("select * from books where bookid = ? LIMIT 1");
			myStmt.setInt(1, id);
			

			rs = myStmt.executeQuery();

			List<Books> l = new ArrayList<Books>();
			while (rs.next()) {
				Books book = new Books();
				book.setBookid(rs.getInt("bookid"));
				book.setName(rs.getString("NAME"));
				book.setAuthor(rs.getString("author"));
				book.setSubject(rs.getString("subject"));
				book.setPrice(rs.getDouble("price"));
				book.setIsbn(rs.getString("isbn"));

				l.add(book);
			}
			return l.get(0);
		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}
	} 
	
	public List<Books> FindBook(final String NAME) {
		PreparedStatement myStmt;
		ResultSet rs = null;
		try {
			myStmt = dbCon.prepareStatement("select * from books where Upper(NAME) LIKE Upper(?)");
			myStmt.setString(1, "%"+NAME+"%");
			

			rs = myStmt.executeQuery();

			List<Books> l = new ArrayList<Books>();
			while (rs.next()) {
				Books book = new Books();
				book.setBookid(rs.getInt("bookid"));
				book.setName(rs.getString("NAME"));
				book.setAuthor(rs.getString("author"));
				book.setSubject(rs.getString("subject"));
				book.setPrice(rs.getDouble("price"));
				book.setIsbn(rs.getString("isbn"));

				l.add(book);
			}
			return l;
		} catch (SQLException e) {
			throw new UnknownDataAccessException(e.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					throw new UnknownDataAccessException(e.getMessage());
				}
			}
		}

		

	}
	
	
}
