package com.sunbeam;
import java.sql.*;

public class DBUtils {
	private static Connection cn;// null
	// add a static method to return SINGLETON (=single instance in the entire Java
	// App) to the caller

	public static Connection fetchConnection() throws SQLException{
		if (cn == null) {
			String url = "jdbc:mysql://localhost:3306/library_management?useSSL=false&allowPublicKeyRetrieval=true";
			cn = DriverManager.getConnection(url, "SUNBEAM", "SUNBEAM");
		}
		return cn;
	}
	//later will add a static method to close connection.

}
